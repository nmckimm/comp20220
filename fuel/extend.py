from math import *

def haversine(long1, lat1, long2, lat2):
    """
    Calculate circle distance using Haversine forumala - using latitude and longitude of two GPS positions.
    :param long1:
    :param lat1:
    :param long2:
    :param lat2:
    :return:
    """

    long1, lat1, long2, lat2 = map(radians, [long1, lat1, long2, lat2])

    latDiff = lat2 - lat1
    longDiff = long2 - long1

    a = sin(latDiff / 2)**2 + cos(lat1) * cos(lat2) * sin(longDiff / 2)**2
    b = 6371
    c = 2 * asin(sqrt(a))
    hav = b * c
    return hav


print(haversine(-122.0368100, 48.9642200,-122.072989, 48.965496))
