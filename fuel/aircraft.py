import math
import csv
from decimal import *

class Aircrafts():

    def __init__(self, type, units, manufacturer, range):
        self.type = type
        self.units = units
        self.manufacturer = manufacturer
        self.range = range

    def aircraftCSV(self, csv2):
        aircrafts = {}
        with open(csv2) as file1:
            csvReader = csv.reader(file1)
            for lines in csvReader:
                aircrafts[lines[0]] = Aircrafts(lines[1], lines[2], lines[3], lines[4])
        file1.close()
        return aircrafts


    def getType(self):
        """
        returns the type of aircraft
        """
        return self.type

    def getUnits(self):
        """
        Returns units of measurement of the aircraft range
        """
        return self.units

    def getManufacturer(self):
        """
        Returns the manufacturer of the aircraft
        """
        return self.manufacturer

    def getRange(self):
        """
        Returns the range of the aircraft in the given unit of measurement
        """

        unitsOfMeasurement = self.units
        if unitsOfMeasurement == 'metric':
            return self.range
        elif unitsOfMeasurement == 'imperial':

            # Using decimals here with rounding to be more accurate than floating point numbers
            return Decimal((Decimal(self.range) * Decimal(1.609344)).quantize(Decimal('0.01'), rounding=ROUND_UP))
        else:
            return self.range

