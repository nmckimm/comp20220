from airport import Airports as Air
from aircraft import Aircrafts as Aircraft
from math import *
from decimal import *
from currency import Currency as Cur
import csv
import numpy as np
from itertools import permutations


def haversine(long1, lat1, long2, lat2):
    """
    Calculate circle distance using Haversine forumala - using latitude and longitude of two GPS positions.
    :param long1:
    :param lat1:
    :param long2:
    :param lat2:
    :return:
    """

    long1, lat1, long2, lat2 = map(radians, [long1, lat1, long2, lat2])

    latDiff = lat2 - lat1
    longDiff = long2 - long1

    a = sin(latDiff / 2)**2 + cos(lat1) * cos(lat2) * sin(longDiff / 2)**2
    b = 6371
    c = 2 * asin(sqrt(a))
    hav = b * c
    return Decimal(hav).quantize(Decimal('0.01'), rounding=ROUND_UP)

def routelistscsv(airports, aircraft):

    with open('input/input.csv', encoding='utf-8-sig') as routes:
        reader = csv.reader(routes)
        airportList = []
        aircraftList = []
        airportKeys1 = []

        for row in reader:
            airportKeys1.extend((row))
            airportKeys1 = airportKeys1[:-1]
            aircraftList.append(row[len(row)-1])
            print(airportKeys1)
            print(aircraftList[0])

            # Important
            # airportPerm is a permutation of the airportsfrom index 1 onwards
            # This generates lists of all possible shuffled variations of the airports
            # Airport 1 & 6 aren't included because they will always remain the same
            # 24 possible variations/flight itineraries
            airportPerm = permutations(airportKeys1[1:])
            # Dict object for IATA airport names
            airportKeys1 = {key: None for key in list(airportKeys1)}
            print("airportKeys1",airportKeys1)
            airportList.append(row[0:-1])
            print("airportList",airportList)

            # This for loop creates a list of lists which is then made into a dictionary (airportList)
            # where the key is the IATA 3-letter airport name and the values are the attributes of that airport
            for i in range(len(row)-1):
                airportInfo1 = []
                for key in airportKeys1.keys():
                    airportInfo = []
                    airportInfo.append(Air.getAirportName(airports.get(key)))
                    airportInfo.append(Air.getCity(airports.get(key)))
                    airportInfo.append(Air.getCountry(airports.get(key)))
                    airportInfo.append(Air.getLat(airports.get(key)))
                    airportInfo.append(Air.getLong(airports.get(key)))
                    airportInfo1.append(airportInfo)

                airportList = dict(zip(airportKeys1.keys(), airportInfo1))

    return airportList, aircraftList, airportPerm

def routelistsmanual(airports, aircrafts):
    """
    Creating an intinerary from user input of individual airports and an aircraft type
    """

    airportKeys = []
    i = 0

    while i < 5:
        airportKeys.append(input("Enter aiport IATA (3-letter) code: "))
        airportKeys[i] = airportKeys[i].replace(" ","")
        airportKeys[i] = airportKeys[i].upper()
        i +=1

    aircraftName = [input("Enter aircraft type eg '747','737','A319'")]

    # Important
    # airportPerm is a permutation of the airportsfrom index 1 onwards
    # This generates lists of all possible shuffled variations of the airports
    # Airport 1 & 6 aren't included because they will always remain the same
    # 24 possible variations/flight itineraries
    airportPerm = permutations(airportKeys[1:])

    # Creating dict object of airports as keys with None Type values
    airportKeys = {key: None for key in list(airportKeys)}

    # This for loop creates a list of lists which is then made into a dictionary (airportList)
    # where the key is the IATA 3-letter airport name and the values are the attributes of that airport
    for i in range(len(airportKeys)):
        airportInfo1 = []
        for key in airportKeys.keys():
            airportInfo = []
            airportInfo.append(Air.getAirportName(airports.get(key)))
            airportInfo.append(Air.getCity(airports.get(key)))
            airportInfo.append(Air.getCountry(airports.get(key)))
            airportInfo.append(Air.getLat(airports.get(key)))
            airportInfo.append(Air.getLong(airports.get(key)))
            airportInfo1.append(airportInfo)

    airportList = dict(zip(airportKeys.keys(), airportInfo1))

    return airportList, aircraftName, airportPerm


def cost(currency1, currency2, distance, fromEuro):
    """
    Function to define the cost of the distance, taking into account currency conversion
    """

    if (currency1 == 'EUR') and (currency2 == 'EUR'):
        cost = 1 * distance
    elif currency1 == 'EUR':
        cost = 1 * distance
    elif currency1 != 'EUR':
        cost = distance * Decimal(fromEuro)
    else:
        return

    return Decimal(cost).quantize(Decimal('0.01'), rounding=ROUND_UP)

def verify():
    if route[0] != route[-1]:
        return "Error, start and end points are not the same"
    elif len(route) < 6:
        return "Error, not enough airports entered"
    elif len(route) > 6:
        return "Error, too many airports entered"
    else:
        return


def main():

    airports = Air.airportCSV(Air, 'input/airport.csv')
    currency = Cur.currencyCSV(Cur, 'input/currencyrates.csv', 'input/countrycurrency.csv')
    aircrafts = Aircraft.aircraftCSV(Aircraft, 'input/aircraft.csv')

    a = airports
    inputType = input("Enter 'a' for manual airport input or 'b' for a preloaded test route: ")
    if inputType == 'a':
        airportDetails, aircraftList, airportCombinations = routelistsmanual(airports,aircrafts)
    elif inputType == 'b':
        airportDetails, aircraftList, airportCombinations = routelistscsv(airports, aircrafts)
    airportKeys = list(airportDetails)

    lowestPrice = 0 # Variable to be reassigned to the lowest itinerary cost
    cheapestRoute = () # Instantiating variable for the cheapest itinerary route

    for route in list(airportCombinations):

        # The first airport must be the beginning and end of each itinerary.
        # startEnd represents the first and last airport and is represented as such so that the individual itinerary
        # (route) could be put in between these two stop points
        startEnd = (airportKeys[0],)
        route = startEnd + route + startEnd
        print("\nThe route is",route)
        routeCost = 0
        counter = 0

        for i in range(0, len(route)-1, 1):
            # Loop to iterate through each segment of itinerary
            # Assigning values to each variable needed to calculate and compare cost of segment
            airport1, airport2 = route[i], route[i+1]
            lat1, long1 = Decimal(airportDetails[route[i]][3]), Decimal(airportDetails[route[i]][4])
            lat2, long2 = Decimal(airportDetails[route[i+1]][3]), Decimal(airportDetails[route[i+1]][4])
            distance = haversine(long1, lat1, long2, lat2)
            aircraftRange = Aircraft.getRange(aircrafts.get(aircraftList[0]))

            # Evaluating range of specified aircraft to see if it can cover the distance between airports
            if int(aircraftRange) < distance:
                print(route[i], "->", route[i + 1], "not feasible")
                print("This path cannot be flown as the aircraft's range of distance is too low")
                break

            # Further variable assignment
            airportCountry1, airportCountry2 = airportDetails[route[i]][2], airportDetails[route[i+1]][2]
            airportCurrency1, airportCurrency2 = Cur.getCurrencyCode(currency.get(airportCountry1)), Cur.getCurrencyCode(
                currency.get(airportCountry2))
            fromEuro = Cur.getRateFromEuro(currency.get(airportCountry1))
            costOfSegment = cost(airportCurrency1, airportCurrency2, distance, fromEuro)

            print(route[i], "->", route[i+1], "€", costOfSegment)

            counter +=1 # counter to ensure all five links are counted in price
            routeCost += costOfSegment # Adding segment cost to evaluate total cost of this path

        if counter == 5:
            if (lowestPrice == 0) or (routeCost < lowestPrice):
                lowestPrice = routeCost
                cheapestRoute = route
            print("Cost for route: €", routeCost)

    print("The cheapest route is:", cheapestRoute, "and it costs €", lowestPrice)


    # Next few rows were used to test my classes and OOP

    #print(Cur.getRateToEuro(currency.get('United Kingdom')))
    #print(type(airports))
    #print(Cur.getCurrencyCode(currency.get('Albania')))
    #print(Cur.getRateFromEuro(currency.get('Albania')))
    #print(Air.getCode(a.get('LHR')))
    #lat1, long1 = Decimal(Air.getLat(a.get('LHR'))), Decimal(Air.getLong(a.get('LHR')))
    #lat2, long2 = Decimal(Air.getLat(a.get('AMS'))), Decimal(Air.getLong(a.get('AMS')))
    #print(lat1, long1, lat2, long2)
    #print(haversine(long1, lat1, long2, lat2))



if __name__ == "__main__":
    main()