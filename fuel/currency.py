import csv
from decimal import *
import sys
import os
import pandas as pd
from collections import OrderedDict, defaultdict

class Currency():

    def __init__(self, currencyCode, rateFromEuro, rateToEuro):
        self.currencyCode = currencyCode
        self.rateFromEuro = rateFromEuro
        self.rateToEuro = rateToEuro

    def currencyCSV(self, csv1, csv2):
        currency = {}
        df = pd.read_csv('input/countrycurrency.csv')
        columns = ['name', 'currency_alphabetic_code', 'fromeuro', 'toeuro']
        df1 = pd.read_csv('input/currencyrates.csv', header=None, names=columns)
        df = df[['name','currency_alphabetic_code']]
        df2 = pd.merge(df,df1, on='currency_alphabetic_code')
        for i in range(len(df2)):
            currency[df2['name_x'][i]] = Currency(df2['currency_alphabetic_code'][i], df2['fromeuro'][i], df2['toeuro'][i])


        return currency

    def getCurrencyCode(self):
        return self.currencyCode

    def getRateFromEuro(self):
        return self.rateFromEuro

    def getRateToEuro(self):
        return self.rateToEuro

def main():
    cwd = os.getcwd()
    print(cwd)
    with open('aircraft.csv', 'r'):
        print("hello")
    os.chdir('/Users/synq/Documents/CS/COMP20230 Data Structures/comp20220/fuel/')
    cwd = os.getcwd()

    print(cwd)
    currency = Currency.currencyCSV(Currency, "input/countrycurrency.csv", "input/currencyrates.csv")
    C = Currency
    c = currency
    print(c.get('EUR'))


if __name__=='__main__':
    main()